# -*- coding: utf-8 -*-
import webapp2
from google.appengine.api import mail


class EnviaCorreo(webapp2.RequestHandler):
  def get(self):
    self.response.headers['Content-Type'] = 'text/plain'
    self.response.write('Hello, ALyES World!')


  def post(self):
    name = self.request.get('name')
    apellidos = self.request.get('apellidos')
    email = self.request.get('email')
    motivo = self.request.get('motivo')
    telefono = self.request.get('telefono')
    comentario = self.request.get('comentario')

    message = mail.EmailMessage()
    message.sender="alberto.ferrero@alyes.es"
    message.subject="Mensaje desde www.alyes.es"
    message.to="alyes@alyes.es"
    message.body="""
    ==================================
    MENSAJE DESDE ALYES
    ==================================

    Nombre: %s

    Apellidos: %s

    E-mail: %s

    Motivo: %s

    Telefono: %s

    Comentario: %s

    """ % (name, apellidos, email, motivo, telefono, comentario)

    message.send()
    self.redirect('gracias.html')



app = webapp2.WSGIApplication([('/enviacorreo', EnviaCorreo)], debug=True)
