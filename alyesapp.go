package alyes

import (
    "bytes"
    "fmt"
    //"strconv"
    "net/http"
    "net/url"
    "regexp"

    "appengine"
    // "appengine/mail"
    "appengine/urlfetch"

)

const mensaje = `
==================================
MENSAJE DESDE ALYES
==================================

Nombre: %s

Apellidos: %s

E-mail: %s

Motivo: %s

Telefono: %s

Comentario: %s
`

func init() {
    //http.HandleFunc("/enviacorreo", handler)
    http.HandleFunc("/enviacorreo", sendMailgunMail)
    http.HandleFunc("/.well-known/acme-challenge/", realizaChallenge)
}

// func handler(w http.ResponseWriter, r *http.Request) {
//     name := r.FormValue("name")
//     apellidos := r.FormValue("apellidos")
//     email := r.FormValue("email")
//     motivo := r.FormValue("motivo")
//     telefono := r.FormValue("telefono")
//     comentario := r.FormValue("comentario")
//
//     c := appengine.NewContext(r)
//     msg := &mail.Message{
//             Sender:  "www.alyes.es <alberto.ferrero@alyes.es>",
//             To:      []string{"alyes@alyes.es"},
//             Subject: "Mensaje desde www.alyes.es",
//             Body:    fmt.Sprintf(mensaje, name,apellidos,email,motivo,telefono,comentario),
//     }
//     if err := mail.Send(c, msg); err != nil {
//             c.Errorf("Couldn't send email: %v", err)
//     }
//
//     http.Redirect(w, r, "gracias.html", 302)
// }

func sendMailgunMail(w http.ResponseWriter, r *http.Request) {
  const mailgunUrl string = "https://api.mailgun.net/v2/mg.alyes.es/messages"
  const username string = "api"
  const password string = "key-4qr0n2kxkrdw0ggffjd2o5eju66y6gu2"

  name := r.FormValue("name")
  apellidos := r.FormValue("apellidos")
  email := r.FormValue("email")
  motivo := r.FormValue("motivo")
  telefono := r.FormValue("telefono")
  comentario := r.FormValue("comentario")

  data := url.Values{}
  data.Set("from", "ALyES (no responder) <webalyes@mg.alyes.es>")
  data.Add("to", "info@alyes.es")
  data.Add("subject", "Mensaje desde www.alyes.es")
  data.Add("text", fmt.Sprintf(mensaje, name,apellidos,email,motivo,telefono,comentario))

  c := appengine.NewContext(r)
  client := urlfetch.Client(c)
  req, err := http.NewRequest("POST", mailgunUrl, bytes.NewBufferString(data.Encode()))
  req.SetBasicAuth(username,password)
  req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
  //req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

  //resp, err := client.Do(req)
  resp, err := client.Do(req)
  if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
  }
  //fmt.Fprintf(w, "HTTP GET returned status %v", resp.Status)
  c.Infof("Envio correo: HTTP GET returned status %v", resp.Status)
  http.Redirect(w, r, "gracias.html", 302)
}



var challenge1 = regexp.MustCompile(`aRxf_D3J4lKXkErYWyuaOAu_s3sCUtsXpO3jMV6mQEU`) 
var response1 = "aRxf_D3J4lKXkErYWyuaOAu_s3sCUtsXpO3jMV6mQEU.6y7PbfC9n2CBTqbXJYkC0my-Nx68S6MY3DjndsYcXQk"

var challenge2 = regexp.MustCompile(`/LsPRo_qOsdOJJ9vyQpYN4EzFFL4qj-h-xZL-CAJ3qzg`)
var response2 = "LsPRo_qOsdOJJ9vyQpYN4EzFFL4qj-h-xZL-CAJ3qzg.6y7PbfC9n2CBTqbXJYkC0my-Nx68S6MY3DjndsYcXQk"

func realizaChallenge(w http.ResponseWriter, r *http.Request) {
  w.Header().Set("Content-Type", "text/plain")
  switch {
    case challenge1.MatchString(r.URL.Path):
      fmt.Fprint(w, response1)
    case challenge2.MatchString(r.URL.Path):
      fmt.Fprint(w, response2)
    default:
      fmt.Fprint(w, "Unknow challenge")
  }
}


